<center>
  <img src="/vsf-full.svg" height="100px" />
</center>

# Vue Storefront 2 integration with Spree

This project is a Spree integration for Vue Storefront 2.

This integration is currently stable and ready for production usage.
If you'd like to report a bug or contribute to the codebase, please see the [repository issues page](https://github.com/vuestorefront/spree/issues).
